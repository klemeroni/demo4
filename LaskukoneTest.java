import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class LaskukoneTest {
    @Test
    public void testKerroLuvulla() {
        Laskukone lk = new Laskukone();
        int correct = lk.annaTulos() * 123;

        lk.kerroLuvulla(96);
        assertEquals(correct, lk.annaTulos());
    }

    @Test
    public void testKorotaPotenssiin() {
        Laskukone lk = new Laskukone();
        int correct = (int) Math.pow(lk.annaTulos(), 8);

        lk.korotaPotenssiin(8);
        assertEquals(correct, lk.annaTulos());
    }

    @Test
    public void testLisaaLuku() {
        Laskukone lk = new Laskukone();
        int correct = lk.annaTulos() + 123;

        lk.lisaaLuku(123);
        assertEquals(correct, lk.annaTulos());
    }

    @Test
    public void testVahennaLuku() {
        Laskukone lk = new Laskukone();
        int correct = lk.annaTulos() - 123;
        
        lk.vahennaLuku(123);
        assertEquals(correct, lk.annaTulos());
    }
}
