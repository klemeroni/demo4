public class Laskukone {
    int tulos;

    public Laskukone() {
        nollaa();
    }

    public void lisaaLuku(int luku) {
        tulos += luku;
    }

    public void vahennaLuku(int luku) {
        tulos -= luku;
    }

    public void kerroLuvulla(int luku) {
        int _tulos = tulos;
        for (int i = 1; i < luku; i++) {
            lisaaLuku(_tulos);
        }
    }

    public void korotaPotenssiin(int luku) {
        int _tulos = tulos;
        for (int i = 1; i < luku; i++) {
            kerroLuvulla(_tulos);
        }
    }

    public int annaTulos() {
        return tulos;
    }

    public void nollaa() {
        tulos = 1;
    }
}